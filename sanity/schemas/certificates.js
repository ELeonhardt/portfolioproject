import { AiOutlineSafetyCertificate as icon } from 'react-icons/ai'

export default {
  name: 'certificate',
  title: 'Certificate',
  type: 'document',
  icon,
  fields: [
    {
      name: 'title',
      title: 'Title',
      type: 'string',
    },
    {
      name: 'slug',
      Title: 'Slug',
      type: 'slug',
      options: {
        source: 'title',
        maxLength: 100,
      },
    },
    {
      name: 'index',
      title: 'Index',
      type: 'number',
      description: 'An index number so the certificates are shown in order',
    },
    {
      name: 'date',
      title: 'Date',
      description: 'Date I obtained the certificate, write in dd/mm/yyyy',
      type: 'string',
    },
    {
      name: 'description',
      title:
        'short description why I took the certificate and of what I learned',
      type: 'text',
    },
    {
      name: 'image',
      title: 'Image',
      description: 'A picture of my certificate',
      type: 'image',
      options: {
        hotspot: true,
      },
    },
  ],
}
