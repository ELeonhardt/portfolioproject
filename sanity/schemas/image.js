import { HiOutlinePhotograph as icon} from "react-icons/hi"

export default {
    name: 'picture',
    title: 'Picture',
    type: 'document',
    icon,
    fields: [
        {
            name: 'altText',
            title: 'Alt Text',
            type: 'string',
        },
        {
            name: 'picture',
            title: 'Picture',
            type: 'image',
            options: {
                hotspot: true,
            },
        }

    ]
    
}