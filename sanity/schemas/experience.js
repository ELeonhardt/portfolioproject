import { GrWorkshop as icon } from 'react-icons/gr'

export default {
  name: 'experience',
  title: 'Experience',
  type: 'document',
  icon,
  fields: [
    {
      name: 'role',
      title: 'Role',
      type: 'string',
    },
    {
      name: 'slug',
      Title: 'Slug',
      type: 'slug',
      options: {
        source: 'role',
        maxLength: 100,
      },
    },
    {
      name: 'index',
      title: 'Index',
      type: 'number',
      description:
        'The job number, so the experience appears in the correct order.',
    },
    {
      name: 'company',
      title: 'Company',
      type: 'string',
      description: 'The company I did work at',
    },
    {
      name: 'timerange',
      title: 'Time Range',
      description: 'Write as month/year - month/year',
      type: 'string',
    },
    {
      name: 'description',
      title: 'short description of my responsabilities',
      type: 'text',
    },
  ],
}
