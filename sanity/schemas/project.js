import { RiFolderChartLine } from 'react-icons/ri'

export default {
  name: 'project',
  title: 'Project',
  type: 'document',
  icon: RiFolderChartLine,
  fields: [
    {
      name: 'name',
      title: 'Project Name',
      type: 'string',
    },
    {
      name: 'slug',
      Title: 'Slug',
      type: 'slug',
      options: {
        source: 'name',
        maxLength: 100,
      },
    },
    {
      name: 'technologies',
      title: 'Technologies',
      type: 'string',
      description: 'The technologies that should appear on the project-card',
    },
    {
      name: 'projectUrl',
      title: 'Project Url',
      type: 'url',
    },
    {
      name: 'repoUrl',
      title: 'Repository Url',
      type: 'url',
    },
    {
      name: 'image',
      title: 'Project Showcase Image',
      type: 'image',
      options: {
        hotspot: true,
      },
    },
    {
      name: 'pageTitle',
      title: 'Page Title',
      type: 'string',
    },
    {
      name: 'subTitle',
      title: 'Page Subtitle',
      type: 'string',
    },
    {
      name: 'introText',
      title: 'Short introduction text',
      type: 'text',
    },
    {
      name: 'problem',
      title: 'The problem to be solved with this project',
      type: 'array',
      of: [
        {
          type: 'block'
        }
      ]
    },
    {
      name: 'role',
      title: 'My role and responsabilities in the project',
      type: 'array',
      of: [
        {
          type: 'block'
        }
      ]
    },
    {
      name: 'challenges',
      title: 'The challenges that arose',
      type: 'array',
      of: [
        {
          type: 'block'
        }
      ]
    },
    {
      name: 'solution',
      title: 'The solution I found',
      type: 'array',
      of: [
        {
          type: 'block'
        }
      ]
    },
    {
      name: 'clientOpinion',
      title: 'The clients opinion',
      type: 'text',
    },
    {
      name: 'clientName',
      title: 'The clients name',
      type: 'string',
    },
    {
      name: 'clientTitle',
      title: 'The clients title, like CEO, designer ect.',
      type: 'string',
    },
    {
      name: 'clientImage',
      title: 'A photo of the client',
      type: 'image',
      options: {
        hotspot: true,
      },
    },
    {
      name: 'applicationImages',
      title: 'Image Gallery',
      type: 'array',
      of: [{ type: 'image' }],
    },
  ],
}