## My programming portfolio page

Welcome! This is my personal homepage to showcase the projects I have been working on. If you need inspiration for your own portfolio, feel free to use what you need. If you have questions, you can send me a message on LinkedIn.

**Technologies used**

    - Gatsby
    - Sanity CMS
    - Styled Components
    - deployed on Netlify

**Resources**
I got lots of inspiration, ideas and knowledge from the following resources:

    - The amazing "Master Gatsby" Course from Wes Bos
    - Kevin Powells CSS Channel for great ideas on design and the menu bar
    - The mindblowing portfolio from Iuri de Paula: https://iuri.is/
