import React from 'react'
import renderer from 'react-test-renderer'
import { testURL } from '../../../jest.config'

import Header from '../Header'

describe('Header', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(<Header siteTitle="Default Starter" />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})

test('some very simple test', () => {
  expect(true).toBe(true)
})
