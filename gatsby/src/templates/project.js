import { graphql } from 'gatsby'
import React from 'react'
import Img from 'gatsby-image'
import styled from 'styled-components'
import { FaGitlab, IoLogoVercel } from 'react-icons/all'
import { PageWrapper } from '../styles/GenericStyles'

// const PageWrapper = styled.div`
//   display: grid;
//   grid-template-columns: minmax(0, 1fr) minmax(0, 1100px) minmax(0, 1fr);
//   .container {
//     grid-column: 2 / 3;
//   }

//   @media (max-width: 1150px) {
//     margin: 0 2em;
//   }
// `

const PartWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 1em;
  justify-content: center;
  align-items: center;

  h1 {
    margin-top: 1em;
  }

  h3 {
    color: var(--lime);
    margin: 0.8em 0;
  }

  ul {
    display: flex;
    gap: 2em;
    align-items: center;
    justify-content: center;
    font-size: 1.2rem;

    p {
      font-size: 0.7rem;
    }

    a {
      color: var(--lime);
    }

    li:hover {
      transform: scale(1.4);
    }
  }

  .text-section {
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    height: 100%;
  }

  .client-image {
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
  }

  & > *:nth-child(1),
  & > *:nth-child(4),
  & > *:nth-child(5),
  & > *:nth-child(8) {
    grid-column: auto / span 2;
  }
  /* For some reason I don't completely understand, some images disappear when hitting this mediaquery */
  /* This code forces the images to show  */
  @media (max-width: 550px) {
    all: unset;
    display: grid;
    grid-template-columns: 1fr;
    grid-gap: 1em;
    & > *:nth-child(3),
    & > *:nth-child(6) {
      grid-column: 1;
      height: 35vh;
    }
    & > *:nth-child(7) {
      grid-column: 1;
    }
  }
`

const Flex = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
`

function project({ data }) {
  const currentProject = data.allSanityProject.nodes[0]
  const headerImageStyle = {
    maxHeight: '25vh',
    objectFit: 'cover',
    width: '100%',
  }
  const imgStyle = {
    objectPosition: '0 -3em',
  }

  const gridImageStyle = {
    width: '100%',
    // maxHeight: '100%',
    objectFit: 'cover',
  }

  const clientImageStyle = {
    width: '8em',
    height: '8em',
    // objectFit: 'cover',
    borderRadius: '50%',
  }
  return (
    <PageWrapper>
      <div className="container">
        <Img
          fluid={currentProject.image.asset.fluid}
          style={headerImageStyle}
          imgStyle={imgStyle}
        />
        <PartWrapper>
          <div>
            <h1>{currentProject.pageTitle}</h1>
            <h3>{currentProject.subTitle}</h3>
            <p>{currentProject.introText}</p>
          </div>
          <ul>
            {currentProject.repoUrl ? (
              <li>
                <a
                  href={currentProject.repoUrl}
                  title="see my code"
                  target="_blank"
                  rel="noreferrer"
                >
                  <FaGitlab />
                </a>
              </li>
            ) : (
              <p>Sorry, the code for this project belongs to the client!</p>
            )}
            <li>
              <a
                href={currentProject.projectUrl}
                title="see this app deployed"
                target="_blank"
                rel="noreferrer"
              >
                <IoLogoVercel />
              </a>
            </li>
          </ul>
          <Img
            fluid={currentProject.applicationImages[0].asset.fluid}
            style={gridImageStyle}
          />
          <div className="text-section">
            <div>
              <h3>The problem </h3>
              <p>{currentProject.problem}</p>
            </div>
            <div>
              <h3>My role</h3>
              <p>{currentProject.role}</p>
            </div>
          </div>
          <div className="text-section">
            <div>
              <h3>The solution</h3>
              <p>{currentProject.solution}</p>
            </div>
            <div>
              <h3>Challenges</h3>
              <p>{currentProject.challenges}</p>
            </div>
          </div>
          <Img
            fluid={currentProject.applicationImages[1].asset.fluid}
            style={gridImageStyle}
          />
          <div className="client-image">
            <Img
              fluid={currentProject.clientImage.asset.fluid}
              style={clientImageStyle}
            />
            <p>
              {currentProject.clientName} <br /> {currentProject.clientTitle}
            </p>
          </div>
          <div>
            <h3>What my client thinks</h3>
            <cite className="client-opinion">
              "{currentProject.clientOpinion}"
            </cite>
          </div>
        </PartWrapper>
      </div>
    </PageWrapper>
  )
}

export default project

// export const query = graphql`
//   query ProjectQuery($slug: String) {
//     allSanityProject(filter: { slug: { current: { eq: $slug } } }) {
//       nodes {
//         name
//         projectUrl
//         repoUrl
//         pageTitle
//         subTitle
//         introText
//         problem
//         role
//         challenges
//         solution
//         clientOpinion
//         clientName
//         clientTitle
//         image {
//           asset {
//             fluid(maxWidth: 1000) {
//               ...GatsbySanityImageFluid
//             }
//           }
//         }
//         applicationImages {
//           asset {
//             fluid(maxWidth: 500) {
//               ...GatsbySanityImageFluid
//             }
//           }
//         }
//         clientImage {
//           asset {
//             fluid(maxWidth: 400) {
//               ...GatsbySanityImageFluid
//             }
//           }
//         }
//       }
//     }
//   }
// `
