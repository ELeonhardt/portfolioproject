import path from 'path'

async function createProjectPages({ actions }) {
  // get a template for the pages
  const todo = path.resolve('./src/templates/todo.js')
  const testbox = path.resolve('./src/templates/testbox.js')
  const photosnap = path.resolve('./src/templates/photosnap.js')

  // I realized a need a individual template for every project, therefore I create the pages this way:
  actions.createPage({
    path: `projects/todo-app`,
    component: todo,
    context: {
      slug: 'todo-app',
    },
  })

  actions.createPage({
    path: `projects/gfg-testbox`,
    component: testbox,
    context: {
      slug: 'gfg-testbox',
    },
  })

  actions.createPage({
    path: `projects/photosnap-website`,
    component: photosnap,
    context: {
      slug: 'photosnap-website',
    },
  })
}

export async function createPages(params) {
  await createProjectPages(params)
}
