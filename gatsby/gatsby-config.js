/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */

import dotenv from 'dotenv'

dotenv.config({ path: '.env' })

export default {
  siteMetadata: {
    title: `Elisabeths Portfolio`,
    description: `Programming portfolio of Elisabeth Leonhardt, for your enjoyment`,
  },
  /* Your site config here */
  plugins: [
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-smoothscroll`,
    'gatsby-plugin-react-helmet',
    {
      resolve: 'gatsby-source-sanity',
      options: {
        projectId: 'o1ozgasm',
        dataset: 'production',
        watchMode: 'true',
        token: process.env.SANITY_TOKEN,
      },
    },
  ],
}
